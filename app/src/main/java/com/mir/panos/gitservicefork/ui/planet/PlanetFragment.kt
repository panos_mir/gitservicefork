package com.mir.panos.gitservicefork.ui.planet

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingComponent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.mir.panos.gitservicefork.AppExecutors
import com.mir.panos.gitservicefork.R
import com.mir.panos.gitservicefork.SignInActivity
import com.mir.panos.gitservicefork.binding.FragmentDataBindingComponent
import com.mir.panos.gitservicefork.databinding.PlanetFragmentBinding
import com.mir.panos.gitservicefork.di.Injectable
import com.mir.panos.gitservicefork.extensions.toUrl
import com.mir.panos.gitservicefork.ui.common.PlanetListAdapter
import com.mir.panos.gitservicefork.util.autoCleared
import com.mir.panos.gitservicefork.vo.Status
import javax.inject.Inject

class PlanetFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    @Inject
    lateinit var auth : FirebaseAuth

    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    var binding by autoCleared<PlanetFragmentBinding>()

    private lateinit var planetViewModel: PlanetViewModel

    private lateinit var adapter: PlanetListAdapter
    private var page: Int = 2

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.planet_fragment,
                container,
                false,
                dataBindingComponent
        )
        if (auth.currentUser == null){
            goToSignInActivity()
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    private fun getPage(): Boolean {
        return !planetViewModel.getPageFromResponse().isEmpty()
    }

    private fun initPlanetList(viewModel: PlanetViewModel) {
        viewModel.results().observe(this, Observer { listResource ->
            binding.planetResource = listResource
            adapter.submitList(listResource?.data)
        })
        binding.swipeLayout.setOnRefreshListener {
            page = 2
            viewModel.forceRemote().observe(this, Observer {
                binding.planetResource = it
                if (it?.data != null) {
                    adapter.submitList(it.data)
                } else
                    adapter.submitList(emptyList())
                binding.swipeLayout.isRefreshing = it?.status == Status.LOADING
            })
        }
        binding.planetsListId.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val lastPosition = layoutManager.findLastVisibleItemPosition()
                if (lastPosition == adapter.itemCount - 1) {
                    if (getPage()) {
                        viewModel.getPlanetsFromNextPage(page).observeForever {
                        }
                        page++
                    }
                }
            }
        })
        binding.executePendingBindings()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        planetViewModel = ViewModelProviders.of(this, viewModelFactory).get(PlanetViewModel::class.java)
        adapter = PlanetListAdapter(dataBindingComponent, appExecutors) {
            //            val planetUrlId = it.planetId.substring(it.planetId.length - 2)
    //            val planetId = planetUrlId.substring(0, planetUrlId.length - 1)
            findNavController().navigate(PlanetFragmentDirections.showPlanet(it.planetId.toUrl(), it.name))
        }
        binding.planetsListId.adapter = adapter
        initPlanetList(planetViewModel)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        googleApiClient.connect()
        when (item.itemId) {
             R.id.logoutMenuButton -> AuthUI.getInstance()
                     .signOut(this.context!!)
                     .addOnCompleteListener{
                         if (it.isSuccessful){
                             goToSignInActivity()
                             Toast.makeText(this.context, "You logged out successfully!", Toast.LENGTH_LONG).show()
                         }
                     }
                 }
        return false
    }

    private fun goToSignInActivity() {
        val intent = Intent(this.context, SignInActivity::class.java)
        startActivity(intent)
        this.activity?.finish()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_toolbar_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

}
