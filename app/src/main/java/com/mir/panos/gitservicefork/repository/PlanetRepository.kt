package com.mir.panos.gitservicefork.repository

import android.arch.lifecycle.LiveData
import com.mir.panos.gitservicefork.AppExecutors
import com.mir.panos.gitservicefork.api.ApiResponse
import com.mir.panos.gitservicefork.api.StarWarsSearchResponse
import com.mir.panos.gitservicefork.api.StarWarsService
import com.mir.panos.gitservicefork.db.PlanetDao
import com.mir.panos.gitservicefork.db.StarWarsDB
import com.mir.panos.gitservicefork.vo.Planet
import com.mir.panos.gitservicefork.vo.Resource
import javax.inject.Inject

class PlanetRepository @Inject constructor(
        private val appExecutors: AppExecutors,
        private val db: StarWarsDB,
        private val planetDao: PlanetDao,
        private val starWarsService: StarWarsService
) {
    private var _next : String = ""
    fun loadPlanets(): LiveData<Resource<List<Planet>>> {
        return object : NetworkBoundResource<List<Planet>, StarWarsSearchResponse>(appExecutors) {
            override fun onFetchFailed() {
                loadFromDb()
            }

            override fun saveCallResult(item: StarWarsSearchResponse) {
                planetDao.insertPlanets(item.items)
                _next = item.next
            }

            override fun shouldFetch(data: List<Planet>?): Boolean {
                return data == null || data.isEmpty()
            }

            override fun createCall(): LiveData<ApiResponse<StarWarsSearchResponse>> = starWarsService.getPlanets()

            override fun loadFromDb() = planetDao.loadPlanets()

        }.asLiveData()
    }

    fun loadPlanetsRemote(): LiveData<Resource<List<Planet>>> {
        return object : NetworkBoundResource<List<Planet>, StarWarsSearchResponse>(appExecutors) {
            override fun onFetchFailed() {
                loadFromDb()
            }

            override fun saveCallResult(item: StarWarsSearchResponse) {
                planetDao.deleteAllPlanets()
                planetDao.insertPlanets(item.items)
                _next = item.next
            }

            override fun shouldFetch(data: List<Planet>?): Boolean {
                return true
            }

            override fun createCall(): LiveData<ApiResponse<StarWarsSearchResponse>> = starWarsService.getPlanets()

            override fun loadFromDb() = planetDao.loadPlanets()

        }.asLiveData()
    }

    fun loadPlanetById(id: Int, planetName: String): LiveData<Resource<Planet>> {
        return object : NetworkBoundResource<Planet, Planet>(appExecutors) {
            override fun saveCallResult(item: Planet) {
                planetDao.insertPlanet(item)
            }

            override fun shouldFetch(data: Planet?): Boolean = data == null

            override fun loadFromDb(): LiveData<Planet> = planetDao.loadPlanetByName(planetName)

            override fun createCall(): LiveData<ApiResponse<Planet>> = starWarsService.getPlanet(id)
        }.asLiveData()
    }

    fun deletePlanet(planet: Planet) {
        appExecutors.diskIO().execute {
            planetDao.deletePlanet(planet)
        }
    }

    fun loadPlanetsNextPage(page: Int) : LiveData<Resource<List<Planet>>> {
        return object : NetworkBoundResource<List<Planet>, StarWarsSearchResponse>(appExecutors){
            override fun saveCallResult(item: StarWarsSearchResponse) {
                planetDao.insertPlanets(item.items)
            }
            override fun shouldFetch(data: List<Planet>?): Boolean = true
            override fun loadFromDb(): LiveData<List<Planet>> = planetDao.loadPlanets()
            override fun createCall(): LiveData<ApiResponse<StarWarsSearchResponse>> = starWarsService.getPlanets(page)
        }.asLiveData()
    }

    fun loadPage(): String{
        return _next
    }

}