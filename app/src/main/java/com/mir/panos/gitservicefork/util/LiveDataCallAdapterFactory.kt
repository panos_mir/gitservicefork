package com.mir.panos.gitservicefork.util

import android.arch.lifecycle.LiveData
import com.mir.panos.gitservicefork.api.ApiResponse
import retrofit2.CallAdapter
import retrofit2.CallAdapter.*
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class LiveDataCallAdapterFactory : Factory() {

    override fun get(
            returnType: Type,
            annotations: Array<out Annotation>,
            retrofit: Retrofit
    ): CallAdapter<*, *>? {
        if (Factory.getRawType(returnType) != LiveData::class.java){
            return null
        }
        val observableType = Factory.getParameterUpperBound(0, returnType as ParameterizedType)
        val rawObservable = Factory.getRawType(observableType)
        if(rawObservable != ApiResponse::class.java){
            throw IllegalArgumentException("type must be a resource")
        }
        if (observableType !is ParameterizedType){
            throw IllegalArgumentException("resource must be parameterized")
        }
        val bodyType = Factory.getParameterUpperBound(0, observableType)

        return LiveDataCallAdapter<Any>(bodyType)
    }
}