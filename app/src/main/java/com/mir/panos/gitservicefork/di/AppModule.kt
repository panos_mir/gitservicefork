package com.mir.panos.gitservicefork.di

import android.app.Application
import android.arch.persistence.room.Room
import com.firebase.ui.auth.AuthUI
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
import com.mir.panos.gitservicefork.GitApp
import com.mir.panos.gitservicefork.R
import com.mir.panos.gitservicefork.api.StarWarsService
import com.mir.panos.gitservicefork.db.PlanetDao
import com.mir.panos.gitservicefork.db.StarWarsDB
import com.mir.panos.gitservicefork.util.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    private val interceptor = HttpLoggingInterceptor()

    @Singleton
    @Provides
    fun provideStarWarsService(retrofit: Retrofit): StarWarsService {
        return retrofit.create(StarWarsService::class.java)
    }

    @Singleton
    @Provides
    fun provideRetrofit(httpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://swapi.co/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .client(httpClient)
                .build()
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): StarWarsDB {
        return Room
                .databaseBuilder(app, StarWarsDB::class.java, "starwars.db")
                .fallbackToDestructiveMigration()
                .build()
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build()
    }

    @Singleton
    @Provides
    fun providePlanetDao(db: StarWarsDB): PlanetDao {
        return db.planetDao()
    }

    @Singleton
    @Provides
    fun provideGoogleSignInOptions(app: Application) : GoogleSignInOptions{
        return GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(app.getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
    }

    @Singleton
    @Provides
    fun provideGoogleApiClient(app: Application, googleSignInOptions: GoogleSignInOptions) : GoogleApiClient{
        return GoogleApiClient.Builder(app)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build()
    }

    @Singleton
    @Provides
    fun provideGoogleSignInClient(app: Application, googleSignInOptions: GoogleSignInOptions): GoogleSignInClient{
        return GoogleSignIn.getClient(app, googleSignInOptions)
    }

    @Singleton
    @Provides
    fun provideAuthUI() : AuthUI{
        return AuthUI.getInstance()
    }

    @Singleton
    @Provides
    fun provideFirebaseAuth() : FirebaseAuth{
        return FirebaseAuth.getInstance()
    }

}