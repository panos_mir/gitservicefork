package com.mir.panos.gitservicefork.vo

enum class Status{
    SUCCESS,
    ERROR,
    LOADING
}