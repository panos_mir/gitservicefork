package com.mir.panos.gitservicefork.ui.planetDetails


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingComponent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mir.panos.gitservicefork.AppExecutors
import com.mir.panos.gitservicefork.R
import com.mir.panos.gitservicefork.binding.FragmentDataBindingComponent
import com.mir.panos.gitservicefork.databinding.PlanetDetailsFragmentBinding
import com.mir.panos.gitservicefork.di.Injectable
import com.mir.panos.gitservicefork.util.autoCleared
import com.mir.panos.gitservicefork.vo.Planet
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 *
 */
class PlanetDetailsFragment : Fragment(), Injectable {

    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)
    var binding by autoCleared<PlanetDetailsFragmentBinding>()

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var planetDetailsViewModel: PlanetDetailsViewModel

    @Inject
    lateinit var appExecutors: AppExecutors

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.planet_details_fragment,
                container,
                false,
                dataBindingComponent
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        planetDetailsViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(PlanetDetailsViewModel::class.java)

        val params = PlanetDetailsFragmentArgs.fromBundle(arguments)
        val planetId = params.planetId
        val planetName = params.planetName
        planetDetailsViewModel.setId(planetId, planetName)

        val planet = planetDetailsViewModel.planet
        planet.observe(this, Observer { resource ->
            binding.planet = resource?.data
            binding.executePendingBindings()
        })
    }

}
