package com.mir.panos.gitservicefork.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.mir.panos.gitservicefork.vo.Planet

@Database(
        entities = [Planet::class],
        version = 4,
        exportSchema = false
)
abstract class StarWarsDB: RoomDatabase(){
    abstract fun planetDao(): PlanetDao
}