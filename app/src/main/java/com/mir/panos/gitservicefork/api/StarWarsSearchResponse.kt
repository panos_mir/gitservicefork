package com.mir.panos.gitservicefork.api

import com.google.gson.annotations.SerializedName
import com.mir.panos.gitservicefork.vo.Planet

data class StarWarsSearchResponse(
        @SerializedName("count")
        val total: Int = 0,
        @field:SerializedName("results")
        val items: List<Planet>,
        @field:SerializedName("next")
        val next: String
)