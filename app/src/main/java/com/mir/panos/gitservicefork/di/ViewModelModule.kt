package com.mir.panos.gitservicefork.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.mir.panos.gitservicefork.ui.planet.PlanetViewModel
import com.mir.panos.gitservicefork.ui.planetDetails.PlanetDetailsViewModel
import com.mir.panos.gitservicefork.viewmodel.StarWarsViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule{

    @Binds
    @IntoMap
    @ViewModelKey(PlanetViewModel::class)
    abstract fun bindPlanetViewModel(planetViewModel: PlanetViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PlanetDetailsViewModel::class)
    abstract fun bindPlanetDetailsViewModel(planetDetailsViewModel: PlanetDetailsViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: StarWarsViewModelFactory): ViewModelProvider.Factory

}