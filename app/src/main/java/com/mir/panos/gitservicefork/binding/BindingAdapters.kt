package com.mir.panos.gitservicefork.binding

import android.databinding.BindingAdapter
import android.view.View
import android.R.attr.visibility



object BindingAdapters {
    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }
}