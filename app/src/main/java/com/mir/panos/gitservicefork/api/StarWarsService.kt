package com.mir.panos.gitservicefork.api

import android.arch.lifecycle.LiveData
import com.mir.panos.gitservicefork.vo.Planet
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface StarWarsService {

    @GET("planets/")
    fun getPlanets() : LiveData<ApiResponse<StarWarsSearchResponse>>

    @GET("planets/")
    fun getPlanets(@Query("page") page: Int) : LiveData<ApiResponse<StarWarsSearchResponse>>

    @GET("planets/{id}")
    fun getPlanet(@Path("id") id: Int) : LiveData<ApiResponse<Planet>>

}