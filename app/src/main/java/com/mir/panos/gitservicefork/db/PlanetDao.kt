package com.mir.panos.gitservicefork.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.mir.panos.gitservicefork.vo.Planet

@Dao
abstract class PlanetDao {

    @Query("SELECT * FROM Planet")
    abstract fun loadPlanets(): LiveData<List<Planet>>

    @Query("SELECT * FROM Planet WHERE name=:planetName")
    abstract fun loadPlanetByName(planetName: String) : LiveData<Planet>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertPlanet(planet: Planet)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertPlanets(planets: List<Planet>)

    @Delete
    abstract fun deletePlanet(planet: Planet)

    @Query("DELETE FROM Planet")
    abstract fun deleteAllPlanets()
}