package com.mir.panos.gitservicefork.di

import com.mir.panos.gitservicefork.MainActivity
import com.mir.panos.gitservicefork.SignInActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeSignInActivity(): SignInActivity
}