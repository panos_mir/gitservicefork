package com.mir.panos.gitservicefork.ui.planet

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.mir.panos.gitservicefork.api.StarWarsSearchResponse
import com.mir.panos.gitservicefork.repository.NetworkBoundResource
import com.mir.panos.gitservicefork.repository.PlanetRepository
import com.mir.panos.gitservicefork.vo.Planet
import com.mir.panos.gitservicefork.vo.Resource
import javax.inject.Inject

class PlanetViewModel @Inject constructor(private val repository: PlanetRepository): ViewModel(){

    fun results() : LiveData<Resource<List<Planet>>> = repository.loadPlanets()
    fun forceRemote() : LiveData<Resource<List<Planet>>> = repository.loadPlanetsRemote()
    fun deletePlanet(planet: Planet)  = repository.deletePlanet(planet)
    fun getPlanetsFromNextPage(page: Int) = repository.loadPlanetsNextPage(page)
    fun getPageFromResponse() = repository.loadPage()
}
