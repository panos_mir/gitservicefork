package com.mir.panos.gitservicefork.extensions

fun String.toUrl() : Int {
    val tmpString = this
    tmpString.substring(this.length-2)
    val urlString = tmpString.substring(0, tmpString.length-1)
    return urlString.toInt()
}