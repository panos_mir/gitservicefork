package com.mir.panos.gitservicefork.di

import com.mir.panos.gitservicefork.ui.planet.PlanetFragment
import com.mir.panos.gitservicefork.ui.planetDetails.PlanetDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributePlanetFragment(): PlanetFragment

    @ContributesAndroidInjector
    abstract fun contributePlanetDetailsFragment(): PlanetDetailsFragment
}