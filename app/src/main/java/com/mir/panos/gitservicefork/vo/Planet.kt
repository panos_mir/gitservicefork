package com.mir.panos.gitservicefork.vo

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Planet(
        @PrimaryKey(autoGenerate = true)
        val id: Int = 0,
        @field:SerializedName("name")
        val name: String,
        @field:SerializedName( "diameter")
        val diameter: String,
        @field:SerializedName("population")
        val population: String,
        @field:SerializedName("url")
        val planetId: String
)