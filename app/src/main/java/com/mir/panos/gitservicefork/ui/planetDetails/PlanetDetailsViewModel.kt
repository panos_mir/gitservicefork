package com.mir.panos.gitservicefork.ui.planetDetails

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.mir.panos.gitservicefork.repository.PlanetRepository
import com.mir.panos.gitservicefork.util.AbsentLiveData
import com.mir.panos.gitservicefork.vo.Planet
import com.mir.panos.gitservicefork.vo.Resource
import javax.inject.Inject

class PlanetDetailsViewModel @Inject constructor(repository: PlanetRepository) : ViewModel() {
    private val _planetLocalId: MutableLiveData<PlanetLocalId> = MutableLiveData()

    val planetLocalId: LiveData<PlanetLocalId>
        get() = _planetLocalId

    val planet: LiveData<Resource<Planet>> = Transformations
            .switchMap(_planetLocalId) { input ->
                input.ifExists { id, planetId ->
                    repository.loadPlanetById(id, planetId)
                }
            }

    fun setId(id: Int?, planetName: String?) {
        val update = PlanetLocalId(id, planetName)
        if (_planetLocalId.value == update)
            return
        _planetLocalId.value = update
    }

    data class PlanetLocalId(val id: Int?, val planetId: String?) {
        fun <T> ifExists(f: (Int, String) -> LiveData<T>): LiveData<T> {
            return if (planetId == null || id==null) {
                AbsentLiveData.create()
            } else {
                f(id, planetId)
            }
        }
    }

}