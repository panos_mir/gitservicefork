package com.mir.panos.gitservicefork.ui.common

import android.databinding.DataBindingComponent
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.mir.panos.gitservicefork.AppExecutors
import com.mir.panos.gitservicefork.R
import com.mir.panos.gitservicefork.databinding.PlanetItemBinding
import com.mir.panos.gitservicefork.vo.ImageHelper
import com.mir.panos.gitservicefork.vo.Planet

class PlanetListAdapter(
        private val dataBindingComponent: DataBindingComponent,
        appExecutors: AppExecutors,
        private val planetClickCallback: ((Planet) -> Unit)?
) : DataBoundListAdapter<Planet, PlanetItemBinding>(
        appExecutors = appExecutors,
        diffCallback = object : DiffUtil.ItemCallback<Planet>() {
            override fun areItemsTheSame(oldItem: Planet, newItem: Planet): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: Planet, newItem: Planet): Boolean {
                return oldItem == newItem
            }
        }
) {
    override fun createBinding(parent: ViewGroup): PlanetItemBinding {
        val binding = DataBindingUtil.inflate<PlanetItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.planet_item,
                parent,
                false,
                dataBindingComponent
        )
        binding.root.setOnClickListener {
            binding.planet?.let {
                planetClickCallback?.invoke(it)
            }
        }
        return binding
    }

    override fun bind(binding: PlanetItemBinding, item: Planet) {
//        val imageHelper = ImageHelper("https://api.adorable.io/avatars/50/${item.name}" + ".png")
        val imageHelper = ImageHelper("https://firebasestorage.googleapis.com/v0/b/gitservicefork.appspot.com/o/download.jpg?alt=media&token=a109b6dc-667d-477d-9e04-40b99733d550")
        binding.planet = item
        binding.imageHelper = imageHelper
    }
}