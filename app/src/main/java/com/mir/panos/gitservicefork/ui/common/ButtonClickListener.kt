package com.mir.panos.gitservicefork.ui.common

interface ButtonClickListener {
    fun onClick()
}